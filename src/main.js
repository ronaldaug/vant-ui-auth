import Vue from 'vue'
import App from './App.vue'
import router from './router'

import Vant from 'vant';
import 'vant/lib/vant-css/index.css';
import {Lazyload,NoticeBar,Loading} from 'vant';
Vue.use(Vant, Lazyload, Loading, NoticeBar);

import './helper'
Vue.config.productionTip = false
new Vue({
    router,
    render: h => h(App)
}).$mount('#app')


