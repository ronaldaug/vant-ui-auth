var firebase = require("firebase/app");

// Add additional services you want to use
require("firebase/firestore");
require("firebase/auth");

var config = {
    /* auth goes here */
};
firebase.initializeApp(config);

export const db = firebase.firestore();
export const auth = firebase.auth();
export const fb = firebase;
